(import
 (except (rnrs base) let-values map)
 (only (guile)
       lambda* λ))

(define member?
  (λ (a lat)
    (cond
     [(null? lat) #f]
     [else
      (or (eq? a (car lat))
          (member? a (cdr lat)))])))

(display
 (simple-format
  #f "(member? 1 '(3 2 1)) -> ~a\n"
  (member? 1 '(3 2 1))))

(display
 (simple-format
  #f "(member? 1 '(3 2 4)) -> ~a\n"
  (member? 1 '(3 2 4))))
