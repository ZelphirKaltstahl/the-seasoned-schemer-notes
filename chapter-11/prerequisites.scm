;;; =============
;;; PREREQUISITES
;;; =============

(library (prerequisites)
  (export atom?
          one?
          pick)
  (import
   (except (rnrs base) let-values map)
   (only (guile)
         lambda* λ))

  (define atom?
    (λ (x)
      (and (not (pair? x))
           (not (null? x)))))

  ;; In theory we could define other things as numbers, for example
  ;; church numerals. The definition of ~one?~ is a layer of
  ;; abstraction over numbers and their representation in code.
  (define one?
    (λ (x)
      ;; In theory we could define other things as numbers, for
      ;; example church numerals.
      (= x 1)))

  (define pick
    (λ (n lat)
      (cond
       [(one? n) (car lat)]
       [else
        (pick (- n 1) (cdr lat))]))))
